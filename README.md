- Created a Simple Streamer project with React-Redux, implementing Routers and GoogleOAuth.
- Fetching APIs
- Sending User into the OAuth Flow
- User Must Sign In and Sign Out via any Google Account
- User can create their Streaming video through OBS.
- Through OBS; User Can go to OBS settings and click the streamer,
  choose costumize, Url: rtmp://localhost/live and put the key of the streamer
  you have created. Then Click ok and start Streaming.
- Video will automatically play in the Browser.